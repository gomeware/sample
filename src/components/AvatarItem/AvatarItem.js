import React from 'react'
import PropTypes from 'prop-types'
import QuestionMark from './question-mark.png'
import './AvatarItem.css'
const AvatarItem = ({
  url,
  status,
  title,
  message,
  footer,
  className,
  ...props
}) => {
  const baseClassName = 'avatar-item'
  return (
    <div className={`${baseClassName} ${className || ''}`} {...props}>
      <div className={`${baseClassName}-image-container`}>
        <img
          src={url || QuestionMark}
          alt=""
          className={`${baseClassName}-main-image`}
        />
        <div className={`${baseClassName}-status ${status || ''}`}>
          {!status ? (
            <img
              src={QuestionMark}
              className={`${baseClassName}-status-empty`}
              alt="empty-status"
            />
          ) : null}
        </div>
      </div>
      {(title || message || footer) && (
        <div className={`${baseClassName}-main-info`}>
          {title && <h3 className={`${baseClassName}-title`}>{title}</h3>}
          {message && (
            <p className={`${baseClassName}-message ellipsis`}>{message}</p>
          )}
          {footer && (
            <p className={`${baseClassName}-footer ellipsis`}>{footer}</p>
          )}
        </div>
      )}
    </div>
  )
}

AvatarItem.propTypes = {
  /** Main Image */
  url: PropTypes.string,
  /** ClassName for status */
  status: PropTypes.string,
  /** Avatar title */
  title: PropTypes.string,
  /** Message displayed on avatar */
  message: PropTypes.string,
  /** Footer displayed on avatar */
  footer: PropTypes.string,
  /** Custom className for the container */
  className: PropTypes.string
}

export default AvatarItem
