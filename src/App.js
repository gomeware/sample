import React from 'react'
import { AvatarItem } from './components'

function App() {
  return (
    <div>
      <div style={{ background: '#2F3B52' }}>
        <AvatarItem
          title="Alexis Miguel Jimenez Olivares Ramirez"
          message="You need to enable JavaScript to run this app."
          footer="This is the footer"
        ></AvatarItem>
      </div>
      <div style={{ background: '#2F3B52', marginTop: 10 }}>
        <AvatarItem
          style={{ width: 200 }}
          url="https://picsum.photos/200"
          title="Alexis Miguel Jimenez Olivares Ramirez"
          message="You need to enable JavaScript to run this app."
          footer="This is the footer and could not be that long"
        ></AvatarItem>
        <span style={{ backgroundColor: 'green' }}>
          Here will be another component.
        </span>
      </div>
      <div style={{ padding: 20 }}>
        Lorem ipsum, dolor sit amet consectetur adipisicing elit. Eligendi
        tenetur libero fugit impedit eius laboriosam accusantium modi facere?
        Omnis, dolore dicta velit aliquam in voluptatibus reprehenderit
        doloribus distinctio eos atque! Quas minima non assumenda earum
        necessitatibus! In, qui ducimus. Minima eum odio architecto cum esse
        quia, omnis, sit culpa magnam amet inventore consectetur nam alias.
        Adipisci nisi ut cumque facilis! Esse totam blanditiis inventore, modi
        earum ipsum quod iste perferendis vel quas non perspiciatis at quaerat
        deleniti quibusdam dolorum placeat accusamus sunt aliquid aperiam
        ratione, eos asperiores ipsa similique. Quo?
      </div>
      <div style={{ padding: 10, borderRadius: 10, background: '#2F3B52' }}>
        <AvatarItem
          style={{ width: 200 }}
          url="https://picsum.photos/300"
          title="Alexis Miguel Jimenez Olivares Ramirez"
          message="You need to enable JavaScript to run this app."
          footer="This is the footer"
        ></AvatarItem>
      </div>
    </div>
  )
}

export default App
